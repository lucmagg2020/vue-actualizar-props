let MyPropsComponent = Vue.component('my-props', {
    template: `<p component="my-props">Valor:{{ myValue }}</p>`,
    data(){
        return{
            myValue: this.propValue
        }
    },
    watch:{
        propValue:function(value){
            this.myValue = value
        }
    },
    props:{
        propValue: { type:String, required:true }
    }

})